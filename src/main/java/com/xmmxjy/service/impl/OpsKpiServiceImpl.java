package com.xmmxjy.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.xmmxjy.entity.OpsKpi;
import com.xmmxjy.mapper.OpsKpiMapper;
import com.xmmxjy.service.IOpsKpiService;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * kpi 服务实现类
 * </p>
 *
 * @author xmm
 * @since 2018-03-20
 */
@Service
public class OpsKpiServiceImpl extends ServiceImpl<OpsKpiMapper, OpsKpi> implements IOpsKpiService {
    @Autowired
    private OpsKpiMapper opsKpiMapper;

    @Override
    public OpsKpi sumVolte(String cgi, Date date, Date nextDate) {
        return opsKpiMapper.sumVolte(cgi,date,nextDate);
    }

    @Override
    public List<Map> getIndexByType24HourAgo(String type,String cgi) {
        return opsKpiMapper.getIndexByType24HourAgo(type,cgi);
    }

    @Override
    public List<OpsKpi> findByCgiAndDateAndStep(String city, String vendor, String cgi, Date date, int length, int step) {
        Date beforeDate = DateUtils.addHours(date,-length + 1 - (step - 1));
        return opsKpiMapper.findByCgiAndDate(city,vendor,cgi,beforeDate,date);
    }

    @Override
    public OpsKpi findByCgi(String cgi, Date date) {
        return opsKpiMapper.findByCgi(cgi, date);
    }

    @Override
    public List<OpsKpi> findByCgiAndDate(String city,String vendor,String cgi, Date date,int length) {
        Date beforeDate = DateUtils.addHours(date,-length + 1);
        return opsKpiMapper.findByCgiAndDate(city,vendor,cgi,beforeDate,date);
    }
}
