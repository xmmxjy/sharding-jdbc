package com.xmmxjy.shardingjdbc;

import com.xiaoleilu.hutool.date.DateUtil;
import com.xmmxjy.entity.OpsKpi;
import com.xmmxjy.service.IOpsKpiService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ShardingJdbcApplication.class)
public class ShardingJdbcApplicationTests {
    @Autowired
    private IOpsKpiService opsKpiService;

    @Test
    public void contextLoads() {
    }

    @Test
    public void kpi() {
        //OpsKpi opsKpi = opsKpiService.selectById(1);
        Date date = DateUtil.parse("2018-02-01 08:00:00","yyyy-MM-dd HH:mm:ss");
        List<OpsKpi> byCgiAndDate = opsKpiService.findByCgiAndDate("贵阳", "华为", null, date, 24);
    }
    @Test
    public void kpiById() {
        String cgi = "460-00-266187-65";
        Date date = DateUtil.parse("2018-02-01 08:00:00","yyyy-MM-dd HH:mm:ss");
        OpsKpi opsKpi = opsKpiService.findByCgi(cgi ,date);
    }

    @Test
    public void insert() {
        OpsKpi kpi = new OpsKpi();
        Date date = DateUtil.parse("2018-02-02 08:00:00","yyyy-MM-dd HH:mm:ss");
        kpi.setBeginTime(date);
        kpi.setCellCgi("sssssnnnn");
        kpi.setCity("贵阳");
        opsKpiService.insert(kpi);
    }
}

