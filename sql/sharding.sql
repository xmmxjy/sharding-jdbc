/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50716
Source Host           : localhost:3306
Source Database       : sharding

Target Server Type    : MYSQL
Target Server Version : 50716
File Encoding         : 65001

Date: 2018-12-18 23:11:17
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for ops_kpi_1
-- ----------------------------
DROP TABLE IF EXISTS `ops_kpi_1`;
CREATE TABLE `ops_kpi_1` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键，自增',
  `begin_time` datetime NOT NULL COMMENT '开始时间',
  `city` varchar(20) DEFAULT NULL COMMENT '地市',
  `district` varchar(20) DEFAULT NULL COMMENT '区县',
  `e_nodeb_name` varchar(100) DEFAULT NULL COMMENT '所属基站（E-NODEB）名称',
  `e_nodeb_id` varchar(30) DEFAULT NULL COMMENT '基站ID',
  `cell_chinese_name` varchar(200) DEFAULT NULL COMMENT '小区中文名',
  `cell_cgi` varchar(50) DEFAULT NULL COMMENT '小区cgi',
  `vender` varchar(20) DEFAULT NULL COMMENT '厂家名称',
  `rrc_conn_est_succ_ratio` double DEFAULT NULL COMMENT 'RRC连接建立成功率',
  `e_rab_est_succ_ratio` double DEFAULT NULL COMMENT 'E-RAB建立成功率',
  `wire_conn_ratio` double DEFAULT NULL COMMENT '无线接通率',
  `rrc_conn_reb_ratio` double DEFAULT NULL COMMENT 'RRC连接重建比率',
  `cell_user_down_avg_delay` double DEFAULT NULL COMMENT '小区用户面下行平均时延',
  `wire_drop_ratio` double DEFAULT NULL COMMENT '无线掉线率',
  `mac_up_error_block_ratio` double DEFAULT NULL COMMENT 'MAC层上行误块率',
  `mac_down_error_block_ratio` double DEFAULT NULL COMMENT 'MAC层下行误块率',
  `up_per_prb_avg_throughput` double DEFAULT NULL COMMENT '上行每PRB平均吞吐量',
  `down_per_prb_avg_throughput` double DEFAULT NULL COMMENT '下行每PRB平均吞吐量',
  `up_prb_avg_use_ratio` double DEFAULT NULL COMMENT '上行PRB平均利用率',
  `down_prb_avg_use_ratio` double DEFAULT NULL COMMENT '下行PRB平均利用率',
  `wire_use_ratio` double DEFAULT NULL COMMENT '无线利用率',
  `change_succ_ratio` double DEFAULT NULL COMMENT '切换成功率',
  `e_rab_jam_ratio` double DEFAULT NULL COMMENT 'E-RAB拥塞率（无线资源不足）',
  `s1_ue_conn_est_succ_ratio` double DEFAULT NULL COMMENT 'S1接口UE相关逻辑信令连接建立成功率',
  `air_up_buss_byte_num` double DEFAULT NULL COMMENT '空口上行业务字节数',
  `air_down_buss_byte_num` double DEFAULT NULL COMMENT '空口下行业务字节数',
  `volte_up_packet_loss_ratio` double DEFAULT NULL COMMENT 'VoLTE上行丢包率',
  `volte_down_packet_loss_ratio` double DEFAULT NULL COMMENT 'VoLTE下行丢包率',
  `volte_down_packet_discard_ratio` double DEFAULT NULL COMMENT 'VoLTE下行弃包率',
  `volte_down_avg_delay` double DEFAULT NULL COMMENT 'VoLTE下行平均时延',
  `volte_voice_traffic` double DEFAULT NULL COMMENT 'VoLTE语音话务量',
  `volte_video_traffic` double DEFAULT NULL COMMENT 'VoLTE视频话务量',
  `volte_voice_peak_user_num` double DEFAULT NULL COMMENT 'VoLTE语音峰值用户数',
  `volte_video_peak_user_num` double DEFAULT NULL COMMENT 'VoLTE视频峰值用户数',
  `lte_2g_change_suc_ratio` double DEFAULT NULL COMMENT 'LTE到2G切换成功率',
  `volte_user_change_suc_ration` double DEFAULT NULL COMMENT 'VoLTE用户切换成功率',
  PRIMARY KEY (`id`),
  KEY `idx_cgi_time` (`city`,`vender`,`cell_cgi`,`begin_time`) USING BTREE,
  KEY `idx_time` (`begin_time`) USING BTREE,
  KEY `idx_cgi_time2` (`cell_cgi`,`begin_time`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='kpi';

-- ----------------------------
-- Records of ops_kpi_1
-- ----------------------------
INSERT INTO `ops_kpi_1` VALUES ('1', '2018-01-01 08:00:00', '贵阳', '白云区', '1BY-阴阳寨一组拉远LHYO', '460-00-266187', '1BY-阴阳寨一组拉远LDHY-ZD-1', '460-00-266187-65', '华为', '0.997748', '1', '0.997748', '0.008375', '500', '0.000665', '0.001514', '0.000876', '0.17', '0.635', '0.1068', '0.087', '0.0905', '0.969072', '0', '1', '57.198822', '1494.245285', '583.461518', '0', '18.257504', '500', '0.4315', '0', '9', '0', '1', '1');

-- ----------------------------
-- Table structure for ops_kpi_2
-- ----------------------------
DROP TABLE IF EXISTS `ops_kpi_2`;
CREATE TABLE `ops_kpi_2` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键，自增',
  `begin_time` datetime NOT NULL COMMENT '开始时间',
  `city` varchar(20) DEFAULT NULL COMMENT '地市',
  `district` varchar(20) DEFAULT NULL COMMENT '区县',
  `e_nodeb_name` varchar(100) DEFAULT NULL COMMENT '所属基站（E-NODEB）名称',
  `e_nodeb_id` varchar(30) DEFAULT NULL COMMENT '基站ID',
  `cell_chinese_name` varchar(200) DEFAULT NULL COMMENT '小区中文名',
  `cell_cgi` varchar(50) DEFAULT NULL COMMENT '小区cgi',
  `vender` varchar(20) DEFAULT NULL COMMENT '厂家名称',
  `rrc_conn_est_succ_ratio` double DEFAULT NULL COMMENT 'RRC连接建立成功率',
  `e_rab_est_succ_ratio` double DEFAULT NULL COMMENT 'E-RAB建立成功率',
  `wire_conn_ratio` double DEFAULT NULL COMMENT '无线接通率',
  `rrc_conn_reb_ratio` double DEFAULT NULL COMMENT 'RRC连接重建比率',
  `cell_user_down_avg_delay` double DEFAULT NULL COMMENT '小区用户面下行平均时延',
  `wire_drop_ratio` double DEFAULT NULL COMMENT '无线掉线率',
  `mac_up_error_block_ratio` double DEFAULT NULL COMMENT 'MAC层上行误块率',
  `mac_down_error_block_ratio` double DEFAULT NULL COMMENT 'MAC层下行误块率',
  `up_per_prb_avg_throughput` double DEFAULT NULL COMMENT '上行每PRB平均吞吐量',
  `down_per_prb_avg_throughput` double DEFAULT NULL COMMENT '下行每PRB平均吞吐量',
  `up_prb_avg_use_ratio` double DEFAULT NULL COMMENT '上行PRB平均利用率',
  `down_prb_avg_use_ratio` double DEFAULT NULL COMMENT '下行PRB平均利用率',
  `wire_use_ratio` double DEFAULT NULL COMMENT '无线利用率',
  `change_succ_ratio` double DEFAULT NULL COMMENT '切换成功率',
  `e_rab_jam_ratio` double DEFAULT NULL COMMENT 'E-RAB拥塞率（无线资源不足）',
  `s1_ue_conn_est_succ_ratio` double DEFAULT NULL COMMENT 'S1接口UE相关逻辑信令连接建立成功率',
  `air_up_buss_byte_num` double DEFAULT NULL COMMENT '空口上行业务字节数',
  `air_down_buss_byte_num` double DEFAULT NULL COMMENT '空口下行业务字节数',
  `volte_up_packet_loss_ratio` double DEFAULT NULL COMMENT 'VoLTE上行丢包率',
  `volte_down_packet_loss_ratio` double DEFAULT NULL COMMENT 'VoLTE下行丢包率',
  `volte_down_packet_discard_ratio` double DEFAULT NULL COMMENT 'VoLTE下行弃包率',
  `volte_down_avg_delay` double DEFAULT NULL COMMENT 'VoLTE下行平均时延',
  `volte_voice_traffic` double DEFAULT NULL COMMENT 'VoLTE语音话务量',
  `volte_video_traffic` double DEFAULT NULL COMMENT 'VoLTE视频话务量',
  `volte_voice_peak_user_num` double DEFAULT NULL COMMENT 'VoLTE语音峰值用户数',
  `volte_video_peak_user_num` double DEFAULT NULL COMMENT 'VoLTE视频峰值用户数',
  `lte_2g_change_suc_ratio` double DEFAULT NULL COMMENT 'LTE到2G切换成功率',
  `volte_user_change_suc_ration` double DEFAULT NULL COMMENT 'VoLTE用户切换成功率',
  PRIMARY KEY (`id`),
  KEY `idx_cgi_time` (`city`,`vender`,`cell_cgi`,`begin_time`) USING BTREE,
  KEY `idx_time` (`begin_time`) USING BTREE,
  KEY `idx_cgi_time2` (`cell_cgi`,`begin_time`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='kpi';

-- ----------------------------
-- Records of ops_kpi_2
-- ----------------------------
INSERT INTO `ops_kpi_2` VALUES ('2', '2018-02-01 08:00:00', '贵阳', '白云区', '1BY-阴阳寨一组拉远LHYO', '460-00-266187', '1BY-阴阳寨一组拉远LDHY-ZD-1', '460-00-266187-65', '华为', '0.997748', '1', '0.997748', '0.008375', '500', '0.000665', '0.001514', '0.000876', '0.17', '0.635', '0.1068', '0.087', '0.0905', '0.969072', '0', '1', '57.198822', '1494.245285', '583.461518', '0', '18.257504', '500', '0.4315', '0', '9', '0', '1', '1');
INSERT INTO `ops_kpi_2` VALUES ('3', '2018-02-02 08:00:00', '贵阳', null, null, null, null, 'sssss', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `ops_kpi_2` VALUES ('4', '2018-02-02 08:00:00', '贵阳', null, null, null, null, 'sssssnnnn', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
