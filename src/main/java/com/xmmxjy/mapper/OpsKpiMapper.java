package com.xmmxjy.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.xmmxjy.entity.OpsKpi;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
  * kpi Mapper 接口
 * </p>
 *
 * @author xmm
 * @since 2018-03-20
 */
public interface OpsKpiMapper extends BaseMapper<OpsKpi> {

    void loadData(@Param("fileName") String fileName);

    OpsKpi sumVolte(@Param("cgi") String cgi, @Param("currentDate") Date date, @Param("nextDate") Date nextDate);

    public List<Map> getIndexByType24HourAgo(@Param("type") String type, @Param("cgi") String cgi);

    List<OpsKpi> findByCgiAndDate(@Param("city") String city, @Param("vendor") String vendor, @Param("cgi") String cgi, @Param("currentDate") Date date, @Param("nextDate") Date nextDate);

    void loadDataTable(@Param("fileName") String fileName, @Param("tableName") String tableName);

    OpsKpi findByCgi(@Param("cgi") String cgi,@Param("date") Date date);
}