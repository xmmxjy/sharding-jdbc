package com.xmmxjy.service;

import com.baomidou.mybatisplus.service.IService;
import com.xmmxjy.entity.OpsKpi;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * kpi 服务类
 * </p>
 *
 * @author xmm
 * @since 2018-03-20
 */
public interface IOpsKpiService extends IService<OpsKpi> {

    OpsKpi sumVolte(String cgi, Date date, Date nextDate);

    List<OpsKpi> findByCgiAndDate(String city, String vendor, String cgi, Date date, int length);
//
   List<Map> getIndexByType24HourAgo(String type, String cgi);

    List<OpsKpi> findByCgiAndDateAndStep(String city, String vendor, String cgi, Date date, int length, int step);

    OpsKpi findByCgi(String cgi, Date date);
}
