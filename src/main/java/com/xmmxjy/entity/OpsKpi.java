package com.xmmxjy.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * kpi
 * </p>
 *
 * @author xmm
 * @since 2018-03-20
 */
@TableName("ops_kpi")
public class OpsKpi extends Model<OpsKpi> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键，自增
     */
	@TableId(value="id", type= IdType.AUTO)
	private Long id;
    /**
     * 开始时间
     */
	@TableField("begin_time")
	private Date beginTime;
    /**
     * 地市
     */
	private String city;
    /**
     * 区县
     */
	private String district;
    /**
     * 所属基站（E-NODEB）名称
     */
	@TableField("e_nodeb_name")
	private String eNodebName;
    /**
     * 基站ID
     */
	@TableField("e_nodeb_id")
	private String eNodebId;
    /**
     * 小区中文名
     */
	@TableField("cell_chinese_name")
	private String cellChineseName;
    /**
     * 小区cgi
     */
	@TableField("cell_cgi")
	private String cellCgi;
    /**
     * 厂家名称
     */
	private String vender;
    /**
     * RRC连接建立成功率
     */
	@TableField("rrc_conn_est_succ_ratio")
	private Double rrcConnEstSuccRatio;
    /**
     * E-RAB建立成功率
     */
	@TableField("e_rab_est_succ_ratio")
	private Double eRabEstSuccRatio;
    /**
     * 无线接通率
     */
	@TableField("wire_conn_ratio")
	private Double wireConnRatio;
    /**
     * RRC连接重建比率
     */
	@TableField("rrc_conn_reb_ratio")
	private Double rrcConnRebRatio;
    /**
     * 小区用户面下行平均时延
     */
	@TableField("cell_user_down_avg_delay")
	private Double cellUserDownAvgDelay;
    /**
     * 无线掉线率
     */
	@TableField("wire_drop_ratio")
	private Double wireDropRatio;
    /**
     * MAC层上行误块率
     */
	@TableField("mac_up_error_block_ratio")
	private Double macUpErrorBlockRatio;
    /**
     * MAC层下行误块率
     */
	@TableField("mac_down_error_block_ratio")
	private Double macDownErrorBlockRatio;
    /**
     * 上行每PRB平均吞吐量
     */
	@TableField("up_per_prb_avg_throughput")
	private Double upPerPrbAvgThroughput;
    /**
     * 下行每PRB平均吞吐量
     */
	@TableField("down_per_prb_avg_throughput")
	private Double downPerPrbAvgThroughput;
    /**
     * 上行PRB平均利用率
     */
	@TableField("up_prb_avg_use_ratio")
	private Double upPrbAvgUseRatio;
    /**
     * 下行PRB平均利用率
     */
	@TableField("down_prb_avg_use_ratio")
	private Double downPrbAvgUseRatio;
    /**
     * 无线利用率
     */
	@TableField("wire_use_ratio")
	private Double wireUseRatio;
    /**
     * 切换成功率
     */
	@TableField("change_succ_ratio")
	private Double changeSuccRatio;
    /**
     * E-RAB拥塞率（无线资源不足）
     */
	@TableField("e_rab_jam_ratio")
	private Double eRabJamRatio;
    /**
     * S1接口UE相关逻辑信令连接建立成功率
     */
	@TableField("s1_ue_conn_est_succ_ratio")
	private Double s1UeConnEstSuccRatio;
    /**
     * 空口上行业务字节数
     */
	@TableField("air_up_buss_byte_num")
	private Double airUpBussByteNum;
    /**
     * 空口下行业务字节数
     */
	@TableField("air_down_buss_byte_num")
	private Double airDownBussByteNum;
    /**
     * VoLTE上行丢包率
     */
	@TableField("volte_up_packet_loss_ratio")
	private Double volteUpPacketLossRatio;
    /**
     * VoLTE下行丢包率
     */
	@TableField("volte_down_packet_loss_ratio")
	private Double volteDownPacketLossRatio;
    /**
     * VoLTE下行弃包率
     */
	@TableField("volte_down_packet_discard_ratio")
	private Double volteDownPacketDiscardRatio;
    /**
     * VoLTE下行平均时延
     */
	@TableField("volte_down_avg_delay")
	private Double volteDownAvgDelay;
    /**
     * VoLTE语音话务量
     */
	@TableField("volte_voice_traffic")
	private Double volteVoiceTraffic;
    /**
     * VoLTE视频话务量
     */
	@TableField("volte_video_traffic")
	private Double volteVideoTraffic;
    /**
     * VoLTE语音峰值用户数
     */
	@TableField("volte_voice_peak_user_num")
	private Double volteVoicePeakUserNum;
    /**
     * VoLTE视频峰值用户数
     */
	@TableField("volte_video_peak_user_num")
	private Double volteVideoPeakUserNum;
    /**
     * LTE到2G切换成功率
     */
	@TableField("lte_2g_change_suc_ratio")
	private Double lte2gChangeSucRatio;
    /**
     * VoLTE用户切换成功率
     */
	@TableField("volte_user_change_suc_ration")
	private Double volteUserChangeSucRation;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String geteNodebName() {
		return eNodebName;
	}

	public void seteNodebName(String eNodebName) {
		this.eNodebName = eNodebName;
	}

	public String geteNodebId() {
		return eNodebId;
	}

	public void seteNodebId(String eNodebId) {
		this.eNodebId = eNodebId;
	}

	public String getCellChineseName() {
		return cellChineseName;
	}

	public void setCellChineseName(String cellChineseName) {
		this.cellChineseName = cellChineseName;
	}

	public String getCellCgi() {
		return cellCgi;
	}

	public void setCellCgi(String cellCgi) {
		this.cellCgi = cellCgi;
	}

	public String getVender() {
		return vender;
	}

	public void setVender(String vender) {
		this.vender = vender;
	}

	public Double getRrcConnEstSuccRatio() {
		return rrcConnEstSuccRatio;
	}

	public void setRrcConnEstSuccRatio(Double rrcConnEstSuccRatio) {
		this.rrcConnEstSuccRatio = rrcConnEstSuccRatio;
	}

	public Double geteRabEstSuccRatio() {
		return eRabEstSuccRatio;
	}

	public void seteRabEstSuccRatio(Double eRabEstSuccRatio) {
		this.eRabEstSuccRatio = eRabEstSuccRatio;
	}

	public Double getWireConnRatio() {
		return wireConnRatio;
	}

	public void setWireConnRatio(Double wireConnRatio) {
		this.wireConnRatio = wireConnRatio;
	}

	public Double getRrcConnRebRatio() {
		return rrcConnRebRatio;
	}

	public void setRrcConnRebRatio(Double rrcConnRebRatio) {
		this.rrcConnRebRatio = rrcConnRebRatio;
	}

	public Double getCellUserDownAvgDelay() {
		return cellUserDownAvgDelay;
	}

	public void setCellUserDownAvgDelay(Double cellUserDownAvgDelay) {
		this.cellUserDownAvgDelay = cellUserDownAvgDelay;
	}

	public Double getWireDropRatio() {
		return wireDropRatio;
	}

	public void setWireDropRatio(Double wireDropRatio) {
		this.wireDropRatio = wireDropRatio;
	}

	public Double getMacUpErrorBlockRatio() {
		return macUpErrorBlockRatio;
	}

	public void setMacUpErrorBlockRatio(Double macUpErrorBlockRatio) {
		this.macUpErrorBlockRatio = macUpErrorBlockRatio;
	}

	public Double getMacDownErrorBlockRatio() {
		return macDownErrorBlockRatio;
	}

	public void setMacDownErrorBlockRatio(Double macDownErrorBlockRatio) {
		this.macDownErrorBlockRatio = macDownErrorBlockRatio;
	}

	public Double getUpPerPrbAvgThroughput() {
		return upPerPrbAvgThroughput;
	}

	public void setUpPerPrbAvgThroughput(Double upPerPrbAvgThroughput) {
		this.upPerPrbAvgThroughput = upPerPrbAvgThroughput;
	}

	public Double getDownPerPrbAvgThroughput() {
		return downPerPrbAvgThroughput;
	}

	public void setDownPerPrbAvgThroughput(Double downPerPrbAvgThroughput) {
		this.downPerPrbAvgThroughput = downPerPrbAvgThroughput;
	}

	public Double getUpPrbAvgUseRatio() {
		return upPrbAvgUseRatio;
	}

	public void setUpPrbAvgUseRatio(Double upPrbAvgUseRatio) {
		this.upPrbAvgUseRatio = upPrbAvgUseRatio;
	}

	public Double getDownPrbAvgUseRatio() {
		return downPrbAvgUseRatio;
	}

	public void setDownPrbAvgUseRatio(Double downPrbAvgUseRatio) {
		this.downPrbAvgUseRatio = downPrbAvgUseRatio;
	}

	public Double getWireUseRatio() {
		return wireUseRatio;
	}

	public void setWireUseRatio(Double wireUseRatio) {
		this.wireUseRatio = wireUseRatio;
	}

	public Double getChangeSuccRatio() {
		return changeSuccRatio;
	}

	public void setChangeSuccRatio(Double changeSuccRatio) {
		this.changeSuccRatio = changeSuccRatio;
	}

	public Double geteRabJamRatio() {
		return eRabJamRatio;
	}

	public void seteRabJamRatio(Double eRabJamRatio) {
		this.eRabJamRatio = eRabJamRatio;
	}

	public Double getS1UeConnEstSuccRatio() {
		return s1UeConnEstSuccRatio;
	}

	public void setS1UeConnEstSuccRatio(Double s1UeConnEstSuccRatio) {
		this.s1UeConnEstSuccRatio = s1UeConnEstSuccRatio;
	}

	public Double getAirUpBussByteNum() {
		return airUpBussByteNum;
	}

	public void setAirUpBussByteNum(Double airUpBussByteNum) {
		this.airUpBussByteNum = airUpBussByteNum;
	}

	public Double getAirDownBussByteNum() {
		return airDownBussByteNum;
	}

	public void setAirDownBussByteNum(Double airDownBussByteNum) {
		this.airDownBussByteNum = airDownBussByteNum;
	}

	public Double getVolteUpPacketLossRatio() {
		return volteUpPacketLossRatio;
	}

	public void setVolteUpPacketLossRatio(Double volteUpPacketLossRatio) {
		this.volteUpPacketLossRatio = volteUpPacketLossRatio;
	}

	public Double getVolteDownPacketLossRatio() {
		return volteDownPacketLossRatio;
	}

	public void setVolteDownPacketLossRatio(Double volteDownPacketLossRatio) {
		this.volteDownPacketLossRatio = volteDownPacketLossRatio;
	}

	public Double getVolteDownPacketDiscardRatio() {
		return volteDownPacketDiscardRatio;
	}

	public void setVolteDownPacketDiscardRatio(Double volteDownPacketDiscardRatio) {
		this.volteDownPacketDiscardRatio = volteDownPacketDiscardRatio;
	}

	public Double getVolteDownAvgDelay() {
		return volteDownAvgDelay;
	}

	public void setVolteDownAvgDelay(Double volteDownAvgDelay) {
		this.volteDownAvgDelay = volteDownAvgDelay;
	}

	public Double getVolteVoiceTraffic() {
		return volteVoiceTraffic;
	}

	public void setVolteVoiceTraffic(Double volteVoiceTraffic) {
		this.volteVoiceTraffic = volteVoiceTraffic;
	}

	public Double getVolteVideoTraffic() {
		return volteVideoTraffic;
	}

	public void setVolteVideoTraffic(Double volteVideoTraffic) {
		this.volteVideoTraffic = volteVideoTraffic;
	}

	public Double getVolteVoicePeakUserNum() {
		return volteVoicePeakUserNum;
	}

	public void setVolteVoicePeakUserNum(Double volteVoicePeakUserNum) {
		this.volteVoicePeakUserNum = volteVoicePeakUserNum;
	}

	public Double getVolteVideoPeakUserNum() {
		return volteVideoPeakUserNum;
	}

	public void setVolteVideoPeakUserNum(Double volteVideoPeakUserNum) {
		this.volteVideoPeakUserNum = volteVideoPeakUserNum;
	}

	public Double getLte2gChangeSucRatio() {
		return lte2gChangeSucRatio;
	}

	public void setLte2gChangeSucRatio(Double lte2gChangeSucRatio) {
		this.lte2gChangeSucRatio = lte2gChangeSucRatio;
	}

	public Double getVolteUserChangeSucRation() {
		return volteUserChangeSucRation;
	}

	public void setVolteUserChangeSucRation(Double volteUserChangeSucRation) {
		this.volteUserChangeSucRation = volteUserChangeSucRation;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "OpsKpi{" +
			", id=" + id +
			", beginTime=" + beginTime +
			", city=" + city +
			", district=" + district +
			", eNodebName=" + eNodebName +
			", eNodebId=" + eNodebId +
			", cellChineseName=" + cellChineseName +
			", cellCgi=" + cellCgi +
			", vender=" + vender +
			", rrcConnEstSuccRatio=" + rrcConnEstSuccRatio +
			", eRabEstSuccRatio=" + eRabEstSuccRatio +
			", wireConnRatio=" + wireConnRatio +
			", rrcConnRebRatio=" + rrcConnRebRatio +
			", cellUserDownAvgDelay=" + cellUserDownAvgDelay +
			", wireDropRatio=" + wireDropRatio +
			", macUpErrorBlockRatio=" + macUpErrorBlockRatio +
			", macDownErrorBlockRatio=" + macDownErrorBlockRatio +
			", upPerPrbAvgThroughput=" + upPerPrbAvgThroughput +
			", downPerPrbAvgThroughput=" + downPerPrbAvgThroughput +
			", upPrbAvgUseRatio=" + upPrbAvgUseRatio +
			", downPrbAvgUseRatio=" + downPrbAvgUseRatio +
			", wireUseRatio=" + wireUseRatio +
			", changeSuccRatio=" + changeSuccRatio +
			", eRabJamRatio=" + eRabJamRatio +
			", s1UeConnEstSuccRatio=" + s1UeConnEstSuccRatio +
			", airUpBussByteNum=" + airUpBussByteNum +
			", airDownBussByteNum=" + airDownBussByteNum +
			", volteUpPacketLossRatio=" + volteUpPacketLossRatio +
			", volteDownPacketLossRatio=" + volteDownPacketLossRatio +
			", volteDownPacketDiscardRatio=" + volteDownPacketDiscardRatio +
			", volteDownAvgDelay=" + volteDownAvgDelay +
			", volteVoiceTraffic=" + volteVoiceTraffic +
			", volteVideoTraffic=" + volteVideoTraffic +
			", volteVoicePeakUserNum=" + volteVoicePeakUserNum +
			", volteVideoPeakUserNum=" + volteVideoPeakUserNum +
			", lte2gChangeSucRatio=" + lte2gChangeSucRatio +
			", volteUserChangeSucRation=" + volteUserChangeSucRation +
			"}";
	}

	public OpsKpi() {
	}

	public OpsKpi(Date beginTime) {
		this.beginTime = beginTime;
	}

	public OpsKpi(Date beginTime, String city, String district, String eNodebName, String eNodebId, String cellChineseName, String cellCgi, String vender, Double rrcConnEstSuccRatio, Double eRabEstSuccRatio, Double wireConnRatio, Double rrcConnRebRatio, Double cellUserDownAvgDelay, Double wireDropRatio, Double macUpErrorBlockRatio, Double macDownErrorBlockRatio, Double upPerPrbAvgThroughput, Double downPerPrbAvgThroughput, Double upPrbAvgUseRatio, Double downPrbAvgUseRatio, Double wireUseRatio, Double changeSuccRatio, Double eRabJamRatio, Double s1UeConnEstSuccRatio, Double airUpBussByteNum, Double airDownBussByteNum, Double volteUpPacketLossRatio, Double volteDownPacketLossRatio, Double volteDownPacketDiscardRatio, Double volteDownAvgDelay, Double volteVoiceTraffic, Double volteVideoTraffic, Double volteVoicePeakUserNum, Double volteVideoPeakUserNum, Double lte2gChangeSucRatio, Double volteUserChangeSucRation) {
		this.beginTime = beginTime;
		this.city = city;
		this.district = district;
		this.eNodebName = eNodebName;
		this.eNodebId = eNodebId;
		this.cellChineseName = cellChineseName;
		this.cellCgi = cellCgi;
		this.vender = vender;
		this.rrcConnEstSuccRatio = rrcConnEstSuccRatio;
		this.eRabEstSuccRatio = eRabEstSuccRatio;
		this.wireConnRatio = wireConnRatio;
		this.rrcConnRebRatio = rrcConnRebRatio;
		this.cellUserDownAvgDelay = cellUserDownAvgDelay;
		this.wireDropRatio = wireDropRatio;
		this.macUpErrorBlockRatio = macUpErrorBlockRatio;
		this.macDownErrorBlockRatio = macDownErrorBlockRatio;
		this.upPerPrbAvgThroughput = upPerPrbAvgThroughput;
		this.downPerPrbAvgThroughput = downPerPrbAvgThroughput;
		this.upPrbAvgUseRatio = upPrbAvgUseRatio;
		this.downPrbAvgUseRatio = downPrbAvgUseRatio;
		this.wireUseRatio = wireUseRatio;
		this.changeSuccRatio = changeSuccRatio;
		this.eRabJamRatio = eRabJamRatio;
		this.s1UeConnEstSuccRatio = s1UeConnEstSuccRatio;
		this.airUpBussByteNum = airUpBussByteNum;
		this.airDownBussByteNum = airDownBussByteNum;
		this.volteUpPacketLossRatio = volteUpPacketLossRatio;
		this.volteDownPacketLossRatio = volteDownPacketLossRatio;
		this.volteDownPacketDiscardRatio = volteDownPacketDiscardRatio;
		this.volteDownAvgDelay = volteDownAvgDelay;
		this.volteVoiceTraffic = volteVoiceTraffic;
		this.volteVideoTraffic = volteVideoTraffic;
		this.volteVoicePeakUserNum = volteVoicePeakUserNum;
		this.volteVideoPeakUserNum = volteVideoPeakUserNum;
		this.lte2gChangeSucRatio = lte2gChangeSucRatio;
		this.volteUserChangeSucRation = volteUserChangeSucRation;
	}
}
