# sharding-jdbc

#### 介绍
简单的sharding-jdbc demo,按时间字段分表示例，单库
实现自定义的分片算法，包含精确分片算法和范围分片算法
可以插入，查询等

#### 软件架构
1. SpringBoot 1.5
2. mybatis-plus
3. sharding-jdbc 3.0.0

#### 安装教程

无

#### 使用说明

1. 导入数据库
2. 导入maven依赖
3. 运行测试类


